"""
Tests related to the LinearTimeInvariantResistor component.
"""

import unittest
import numpy as np

from canlib.components.lin_inv_resistor import LinearTimeInvariantResistor


class TestLinearTimeInvariantResistor(unittest.TestCase):
    """
    Tests related to the LinearTimeInvariantResistor component.
    """

    def test_constructor(self):
        """
        LinearTimeInvariantResistor should have its attributes initialized
        correctly.
        """
        resistor = LinearTimeInvariantResistor(1, 2, 1000)
        self.assertEqual(resistor.node_a, 1)
        self.assertEqual(resistor.node_b, 2)
        self.assertEqual(resistor.resistance, 1000)

    def test_invalid_resistance(self):
        """
        LinearTimeInvariantResistor should reject zero valued resistance.
        """
        with self.assertRaises(ValueError) as context:
            resistor = LinearTimeInvariantResistor(1, 2, 0)
            self.assertTrue(
                'LinearTimeInvariantResistor eletrical resistance cannot be zero!' in context.exception)
