"""
This module defines a linear time-invariant two-terminal resistor.
"""

import numpy as np


class LinearTimeInvariantResistor:
    """
    Defines a linear time-invariant two-terminal resistor.
    Attributes
    ----------
    node_a : int
        Specifies the position of the component's first terminal on the
        Condutance Matrix.
    node_b : int
        Specifies the position of the component's second terminal on the
        Condutance Matrix.
    resistance: float
        Specifies the component's eletrical resistance in ohms (Ω).

    Raises
    ------
    ValueError
        If the resistance attribute is zero.
    """

    def __init__(self, node_a: int, node_b: int, resistance: float):
        self.node_a = node_a
        self.node_b = node_b
        if resistance == 0:
            raise ValueError(
                'LinearTimeInvariantResistor eletrical resistance cannot be zero!')
        else:
            self.resistance = resistance

    def print_time_domain_analysis(
            self,
            condutance_matrix: np.array,
            *_) -> None:
        """
        Prints the component's parameters on a Condutance Matrix and on its
        Current Vector for time domain analysis
        """
        condutance_matrix[self.node_a][self.node_a] = condutance_matrix[self.node_a][self.node_a] + 1/self.resistance
        condutance_matrix[self.node_a][self.node_b] = condutance_matrix[self.node_a][self.node_b] - 1/self.resistance
        condutance_matrix[self.node_b][self.node_a] = condutance_matrix[self.node_b][self.node_a] - 1/self.resistance
        condutance_matrix[self.node_b][self.node_b] = condutance_matrix[self.node_b][self.node_b] + 1/self.resistance

    def print_steady_state_analysis(
            self,
            condutance_matrix: np.array,
            *_) -> None:
        """
        Prints the component's parameters on a Condutance Matrix and on its
        Current Vector for steady state analysis
        """
        condutance_matrix[self.node_a][self.node_a] = condutance_matrix[self.node_a][self.node_a] + 1/self.resistance
        condutance_matrix[self.node_a][self.node_b] = condutance_matrix[self.node_a][self.node_b] - 1/self.resistance
        condutance_matrix[self.node_b][self.node_a] = condutance_matrix[self.node_b][self.node_a] - 1/self.resistance
        condutance_matrix[self.node_b][self.node_b] = condutance_matrix[self.node_b][self.node_b] + 1/self.resistance
